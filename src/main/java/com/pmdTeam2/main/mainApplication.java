package com.pmdTeam2.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication(scanBasePackages = "com.pmdTeam2")
@EnableJpaRepositories("com.pmdTeam2.repository")
@EntityScan("com.pmdTeam2.entity")
public class mainApplication {

	public static void main(String[] args) {
		SpringApplication.run(mainApplication.class, args);
		System.out.println(bcrypt("123"));
	}
	@Bean
	public static PasswordEncoder passwordEncoder() {
	    return new BCryptPasswordEncoder();
	}
	public static  String bcrypt(String str) {
		String code = passwordEncoder().encode(str);
		return code;
	}
}

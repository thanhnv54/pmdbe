package com.pmdTeam2.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.pmdTeam2.entity.LogUpload;
import com.pmdTeam2.entity.User;
import com.pmdTeam2.entity.XmlFile;
import com.pmdTeam2.entity.report;
import com.pmdTeam2.repository.LogUploadRepository;
import com.pmdTeam2.repository.UserRepository;
import com.pmdTeam2.repository.XmlFileRepository;
import com.pmdTeam2.repository.reportRepository;
import com.pmdTeam2.service.PmdService;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api/file")
public class readxmlController {

	public static Logger log = LoggerFactory.getLogger(readxmlController.class);

	@Autowired
	PmdService pmdService;

	@Autowired
	reportRepository reportRepo;

	@Autowired
	XmlFileRepository Xmlrepo;

	@Autowired
	LogUploadRepository logrepo;

	@Autowired
	UserRepository userRepo;

	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
		pmdService.uploadFile(file);

		return ResponseEntity.ok().body("successed upload");
	}

	@RequestMapping(value = "/download/{id}", method = RequestMethod.GET)
	public void download2(@PathVariable(value = "id") long id, HttpServletRequest request,
			HttpServletResponse response) {

		String pattern = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		String date = simpleDateFormat.format(new Date());

		// Path file= Paths.get( PmdService.filexml.getFilename());
		XmlFile c = Xmlrepo.findById(id);
		Path file = Paths.get(c.getFilename());

		if (Files.exists(file)) {
			response.setContentType("application/xml");
			response.addHeader("Content-disposition",
					"attachment; filename=" + date.toString().replaceAll("[^a-zA-Z0-9]", "_") + "_" + "report.xml");
			try {
				Files.copy(file, response.getOutputStream());
				response.getOutputStream().flush();
				System.out.println("Download success");
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("try again");
			}

		}

	}
	
	public List<Integer> getpageAfterFilter(List<?> lst) {

		int size = lst.size();

		int totalpages = (size / 10);

		if (size % 10 > 0) {
			++totalpages;
		}
		List<Integer> list = new ArrayList<Integer>();
		for (int i = 1; i <= totalpages; i++) {
			list.add(i);
		}
	 return list;
	}
	
	public static List<Integer> pageNumber;
	
	@RequestMapping(value = "/paging/{id}/{page}/{sorttype}", method = RequestMethod.GET)
	public Object[] paging(@PathVariable(value = "id") long id, @PathVariable(value = "page") int page,
			@PathVariable(value = "sorttype") String sorttype) {
		
		List<report>  reportList = reportRepo.findAllreportbyid(id);
		//List<report> sortList;
		
		switch (sorttype) {
		case "undefined":
			pageNumber =getpageAfterFilter(reportList);
			break;
		case "1":
			reportList = reportList.stream().filter(report -> "1".equals(report.getPriority()))
					.collect(Collectors.toList());
			pageNumber =getpageAfterFilter(reportList);
			break;
		case "2":
			reportList = reportList.stream().filter(report -> "2".equals(report.getPriority()))
					.collect(Collectors.toList());
			pageNumber = getpageAfterFilter(reportList);
			break;
		case "3":
			reportList = reportList.stream().filter(report -> "3".equals(report.getPriority()))
					.collect(Collectors.toList());
			pageNumber =getpageAfterFilter(reportList);
			break;
		case "4":
			reportList = reportList.stream().filter(report -> "4".equals(report.getPriority()))
					.collect(Collectors.toList());
			pageNumber =getpageAfterFilter(reportList);
			break;
		case "5":
			reportList = reportList.stream().filter(report -> "5".equals(report.getPriority()))
					.collect(Collectors.toList());
			pageNumber =getpageAfterFilter(reportList);
			break;
		default:
			pageNumber = getpageAfterFilter(reportList);
			break;
		}
		int listszie = reportList.size();

		int pageend = page * 10;

		int pagestart = pageend - 10;
		if (listszie - pageend < 0) {
			int temp = Math.abs(listszie - pageend);
			pageend = pageend - temp;
			pagestart = pageend - pageend % 10;
		}

		List<report> pagesize = reportList.subList(pagestart, pageend);
		System.out.println("pagesize");
		Object[] obj = new Object[3];
		obj[1] = pagesize;
		obj[0] = pageNumber;
		obj[2] = sorttype;
		return obj;
	}

	@RequestMapping(value = "/getname/{id}", method = RequestMethod.GET)
	public LogUpload getname(@PathVariable(value = "id") long id) {
		LogUpload a = logrepo.findnamebyid(id);
		return a;
	}

	@GetMapping(value = "/logs")
	public List<LogUpload> getListLogUpload() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String name = authentication.getName();
		Optional<User> user = userRepo.findByUsername(name);
		List<LogUpload> list = logrepo.listLogUpload(user.get().getId());
		return list;
	}

	@GetMapping(value = "/reports/{id}")
	public List<report> getListReports(@PathVariable(value = "id") long id) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String name = authentication.getName();
		List<report> rp = reportRepo.findByIdxmlfile(id);
		return rp;
	}

}

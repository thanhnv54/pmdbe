package com.pmdTeam2.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.pmdTeam2.entity.LogUpload;

@Repository
public interface LogUploadRepository extends JpaRepository<LogUpload, Long> {
	@Query (value="SELECT * FROM logupload u WHERE u.filename = ?1" ,nativeQuery=true)
	public LogUpload findLogUploadByFileName(String filename);
	

	@Query(value = "SELECT * FROM logupload l WHERE l.user_id = ?1", nativeQuery = true)
	public List<LogUpload> listLogUpload(long id);

	@Query(value="SELECT * FROM logupload u WHERE u.id = ?1" , nativeQuery = true)
	LogUpload findnamebyid(long id); 
	
	@Query(value = "UPDATE logupload SET totalerr = ?2 WHERE id = ?1", nativeQuery = true)
	public boolean updateTotalError(long id, int err);
	
}

package com.pmdTeam2.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.pmdTeam2.entity.XmlFile;
import com.pmdTeam2.entity.report;

@Repository
public interface XmlFileRepository extends JpaRepository<XmlFile, Long> {
	
	
	@Query(value = "SELECT x.* FROM xmlfile x JOIN report r on r.xml_File_id = x.id JOIN logupload l on l.id = x.log_upload_id  where l.id=?1", nativeQuery = true)
	XmlFile findById(long id);
	

}

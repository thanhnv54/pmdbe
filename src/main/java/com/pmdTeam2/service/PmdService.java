package com.pmdTeam2.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.pmdTeam2.entity.LogUpload;
import com.pmdTeam2.entity.User;
import com.pmdTeam2.entity.XmlFile;
import com.pmdTeam2.entity.report;
import com.pmdTeam2.repository.LogUploadRepository;
import com.pmdTeam2.repository.UserRepository;
import com.pmdTeam2.repository.XmlFileRepository;
import com.pmdTeam2.repository.reportRepository;

import net.sourceforge.pmd.PMD;

@Service
public class PmdService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private reportRepository reportRepo;

	@Autowired
	private LogUploadRepository logUploadRepository;

	@Autowired
	private XmlFileRepository xmlFileRepository;

	public static XmlFile filexml;
	public static long idc;
	public static long idup;
	public static String pathFileJava = "";
	
	
	public void exportXML(String fileName) throws IOException {

		String javaFile = fileName;
		String xmlFile = javaFile.substring(0, javaFile.lastIndexOf('.'));
		Path pathxml = Paths.get("xml_file").toAbsolutePath();
		Path pathjava = Paths.get("uploaded_file").toAbsolutePath();
		String[] arguments = { "-d", pathjava.toString()+"\\" + javaFile, "-f", "xml", "-R", "\\rule\\javarule.xml", "-r",
				pathxml.toString()+"\\" + xmlFile + ".xml" };
		PMD.run(arguments);
		LogUpload logUpload = logUploadRepository.findLogUploadByFileName(pathjava.toString()+"\\" + javaFile);
		System.out.println(logUpload.getId());
		XmlFile xmlFile2 = new XmlFile(pathxml.toString()+"\\" + xmlFile + ".xml", logUpload);
		XmlFile xmlf;
		xmlf = xmlFileRepository.save(xmlFile2);
		idc = xmlf.getId();
		
		try {
			filexml = xmlf;
			
			report(xmlf);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public ResponseEntity<Object> uploadFile(MultipartFile file) throws IOException {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();
		User user = userRepository.findByUsername(username).get();
		
		String pattern = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		String date = simpleDateFormat.format(new Date());
		String getFileName = date.toString().replaceAll("[^a-zA-Z0-9]", "_") + "_" + file.getOriginalFilename();
		
		Path path = Paths.get("uploaded_file").toAbsolutePath();
		if(!Files.exists(path)) {
            Files.createDirectories(path);
		}
		
		InputStream is = file.getInputStream();
		pathFileJava = path.toString() + "\\" + getFileName;
		Files.copy(is, Paths.get(pathFileJava), StandardCopyOption.REPLACE_EXISTING);
		
		//File uploadedFile = new File("\\updown\\", getFileName);
		//String fileName = "\\updown\\" + getFileName;
		//			uploadedFile.createNewFile();
		//			FileOutputStream fileOutputStream = new FileOutputStream(uploadedFile);
		//			fileOutputStream.write(file.getBytes());
		//			fileOutputStream.close();
					LogUpload logUpload = new LogUpload(pathFileJava, user);
					LogUpload logup;
				    logup=logUploadRepository.save(logUpload);
				    idup = logup.getId();
					exportXML(getFileName);
		return new ResponseEntity<Object>("file Uplaoded succesfully", HttpStatus.OK);
	}
	
	

	// read xml file to DB
	
	public static List<String> listpath;

	public void report(XmlFile idXml) throws ParserConfigurationException, SAXException, IOException {
		File f = new File(idXml.getFilename());
		
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder buider = factory.newDocumentBuilder();
		Document doc = buider.parse(f);

		Element file = doc.getDocumentElement();
		NodeList violationList = file.getElementsByTagName("violation");

		report b = new report();
		int totalerr = 0;
		NodeList fileList = file.getElementsByTagName("file");

		for(int j = 0; j < fileList.getLength(); j++) {
			Node node1= fileList.item(j);
			
			if(node1.getNodeType() == Node.ELEMENT_NODE) {
				
				Element filcontent = (Element) node1;
				String path =filcontent.getAttribute("name");

				for (int i = 0; i < violationList.getLength(); i++) {
					Node node = violationList.item(i);
				
					if (node.getNodeType() == Node.ELEMENT_NODE) {
				
						

						Element violation = (Element) node;
				
						String beginline = violation.getAttribute("beginline");
						String endline = violation.getAttribute("endline");
						String begincolumn = violation.getAttribute("begincolumn");
						String endcolumn = violation.getAttribute("endcolumn");
						String rule = violation.getAttribute("rule");
						String ruleset = violation.getAttribute("ruleset");
						String packages = violation.getAttribute("package");
						String class_z = violation.getAttribute("class");
						String temp= violation.getTextContent();
						String externalInfoUrl = violation.getAttribute("externalInfoUrl");
						String priority = violation.getAttribute("priority");
						b = new report(beginline, endline, begincolumn, endcolumn, rule,path,  ruleset, packages, class_z,
								externalInfoUrl,temp, idXml, priority);
					}
					totalerr++;
					reportRepo.save(b);
				}
				
			}
		}
		logUploadRepository.updateTotalError(idup, totalerr);
	}
	
	

}

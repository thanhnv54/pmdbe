package com.pmdTeam2.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "logupload")
public class LogUpload {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column
	private String filename;

	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "user_id", nullable = false)
	private User user;

	@OneToOne(mappedBy = "logUpload")
	@JsonIgnore
    private XmlFile xmlFile;
	
	@Column(nullable = true)
	private int totalerr;
	
	
	
	public int getTotalerr() {
		return totalerr;
	}

	public void setTotalerr(int totalerr) {
		this.totalerr = totalerr;
	}

	public XmlFile getXmlFile() {
		return xmlFile;
	}

	public void setXmlFile(XmlFile xmlFile) {
		this.xmlFile = xmlFile;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public LogUpload(long id, String filename, User user) {
		super();
		this.id = id;
		this.filename = filename;
		this.user = user;
	}
	
	public LogUpload(String filename, User user) {
		super();
		this.filename = filename;
		this.user = user;
	}
	
	public LogUpload(long id, String filename, User user, XmlFile xmlFile) {
		super();
		this.id = id;
		this.filename = filename;
		this.user = user;
		this.xmlFile = xmlFile;
	}

	
	
	public LogUpload(String filename, User user, XmlFile xmlFile, int totalerr) {
		super();
		this.filename = filename;
		this.user = user;
		this.xmlFile = xmlFile;
		this.totalerr = totalerr;
	}

	public LogUpload(long id, String filename, User user, XmlFile xmlFile, int totalerr) {
		super();
		this.id = id;
		this.filename = filename;
		this.user = user;
		this.xmlFile = xmlFile;
		this.totalerr = totalerr;
	}

	public LogUpload() {
		super();
	}
	
	
}

package com.pmdTeam2.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "report")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class report implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, length = 11, nullable = false)
	private long id;

	@Column(name = "beginline", nullable = true)
	private String beginline;

	@Column(name = "endline", nullable = true)
	private String endline;

	@Column(name = "begincolumn", nullable = true)
	private String begincolumn;

	@Column(name = "endcolumn", nullable = true)
	private String endcolumn;

	@Column(name = "rule", nullable = true)
	private String rule;
	
	@Column(name = "priority", nullable = true)
	private String priority;

	public report() {
		id = 0;
	}
	
	@Column(name = "path", nullable = true)
	private String path;

	public report(String beginline, String endline, String begincolumn, String endcolumn, String rule, String path,
			String ruleset, String packages, String class_z, String externalInfoUrl, String textcontent,
			XmlFile xmlFile, String priority) {
		super();
		this.beginline = beginline;
		this.endline = endline;
		this.begincolumn = begincolumn;
		this.endcolumn = endcolumn;
		this.rule = rule;
		this.path = path;
		this.ruleset = ruleset;
		this.packages = packages;
		this.class_z = class_z;
		this.externalInfoUrl = externalInfoUrl;
		this.textcontent = textcontent;
		this.xmlFile = xmlFile;
		this.priority = priority;
	}



	public String getPath() {
		return path;
	}



	public void setPath(String path) {
		this.path = path;
	}

	@Column(name = "ruleset", nullable = true)
	private String ruleset;

	@Column(name = "package", nullable = true)
	private String packages;

	@Column(name = "class", nullable = true)
	private String class_z;

	@Column(name = "externalInfoUrl", nullable = true)
	private String externalInfoUrl;
	
	@Column(name = "textcontent", nullable = false)
	private String textcontent;
	

	public report(String beginline, String endline, String begincolumn, String endcolumn, String rule, String ruleset,
			String packages, String class_z, String externalInfoUrl, String textcontent, XmlFile xmlFile,
			String priority) {
		super();
		this.beginline = beginline;
		this.endline = endline;
		this.begincolumn = begincolumn;
		this.endcolumn = endcolumn;
		this.rule = rule;
		this.ruleset = ruleset;
		this.packages = packages;
		this.class_z = class_z;
		this.externalInfoUrl = externalInfoUrl;
		this.textcontent = textcontent;
		this.xmlFile = xmlFile;
		this.priority = priority;
	}



	public String getTextcontent() {
		return textcontent;
	}



	public void setTextcontent(String textcontent) {
		this.textcontent = textcontent;
	}

	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "xmlFile_id", nullable = false)
	private XmlFile xmlFile;
	
	

	public report(String beginline, String endline, String begincolumn, String endcolumn, String rule, String ruleset,
			String packages, String class_z, String externalInfoUrl, XmlFile xmlFile, String priority) {
		super();
		this.beginline = beginline;
		this.endline = endline;
		this.begincolumn = begincolumn;
		this.endcolumn = endcolumn;
		this.rule = rule;
		this.ruleset = ruleset;
		this.packages = packages;
		this.class_z = class_z;
		this.externalInfoUrl = externalInfoUrl;
		this.xmlFile = xmlFile;
		this.priority = priority;
	}
	
	

	public report(long id, String beginline, String endline, String begincolumn, String endcolumn, String rule,
			String ruleset, String packages, String class_z, String externalInfoUrl, XmlFile xmlFile, String priority) {
		super();
		this.id = id;
		this.beginline = beginline;
		this.endline = endline;
		this.begincolumn = begincolumn;
		this.endcolumn = endcolumn;
		this.rule = rule;
		this.ruleset = ruleset;
		this.packages = packages;
		this.class_z = class_z;
		this.externalInfoUrl = externalInfoUrl;
		this.xmlFile = xmlFile;
		this.priority = priority;
	}



	public XmlFile getXmlFile() {
		return xmlFile;
	}

	public void setXmlFile(XmlFile xmlFile) {
		this.xmlFile = xmlFile;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getBeginline() {
		return beginline;
	}

	public void setBeginline(String beginline) {
		this.beginline = beginline;
	}

	public String getEndline() {
		return endline;
	}

	public void setEndline(String endline) {
		this.endline = endline;
	}

	public String getBegincolumn() {
		return begincolumn;
	}

	public void setBegincolumn(String begincolumn) {
		this.begincolumn = begincolumn;
	}

	public String getEndcolumn() {
		return endcolumn;
	}

	public void setEndcolumn(String endcolumn) {
		this.endcolumn = endcolumn;
	}

	public String getRule() {
		return rule;
	}

	public void setRule(String rule) {
		this.rule = rule;
	}

	public String getRuleset() {
		return ruleset;
	}

	public void setRuleset(String ruleset) {
		this.ruleset = ruleset;
	}

	public String getPackages() {
		return packages;
	}

	public void setPackages(String packages) {
		this.packages = packages;
	}

	public String getClass_z() {
		return class_z;
	}

	public void setClass_z(String class_z) {
		this.class_z = class_z;
	}

	public String getExternalInfoUrl() {
		return externalInfoUrl;
	}

	public void setExternalInfoUrl(String externalInfoUrl) {
		this.externalInfoUrl = externalInfoUrl;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}



}

package com.pmdTeam2.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "xmlfile")
public class XmlFile {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column
	private String filename;

	@OneToOne
    @JoinColumn(name = "logUpload_id")
    private LogUpload logUpload;
	
	 @JsonIgnore
	 @OneToMany(fetch = FetchType.LAZY, mappedBy = "xmlFile")
	 private Set<report> repor = new HashSet<>();

	public long getId() {
		return id;
	}

	public XmlFile(String filename, LogUpload logUpload) {
		super();
		this.filename = filename;
		this.logUpload = logUpload;
	}

	public Set<report> getReport() {
		return repor;
	}

	public void setReport(Set<report> report) {
		this.repor = report;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public LogUpload getLogUpload() {
		return logUpload;
	}

	public void setLogUpload(LogUpload logUpload) {
		this.logUpload = logUpload;
	}


	public XmlFile(String filename, LogUpload logUpload, Set<com.pmdTeam2.entity.report> report) {
		super();
		this.filename = filename;
		this.logUpload = logUpload;
		this.repor = report;
	}

	public XmlFile(long id, String filename, LogUpload logUpload, Set<com.pmdTeam2.entity.report> report) {
		super();
		this.id = id;
		this.filename = filename;
		this.logUpload = logUpload;
		this.repor = report;
	}

	public XmlFile() {
		super();
	}
	
	
}
